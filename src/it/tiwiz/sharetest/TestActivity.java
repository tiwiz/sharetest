package it.tiwiz.sharetest;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ShareActionProvider;

public class TestActivity extends Activity implements OnClickListener{
	
	EditText txtShareMessage;
	Button btnNormalShare;
	
	ShareActionProvider sap;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_test);
		
		txtShareMessage = (EditText) findViewById(R.id.txtShare);
		btnNormalShare = (Button) findViewById(R.id.btnShare);
		
		btnNormalShare.setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.test, menu);
		
		MenuItem shareMenuItem = menu.findItem(R.id.menu_item_share);
		
		sap = (ShareActionProvider) shareMenuItem.getActionProvider();
		sap.setShareHistoryFileName(ShareActionProvider.DEFAULT_SHARE_HISTORY_FILE_NAME);
		sap.setShareIntent(getShareIntent());
		
		return true;
	}
	
	private Intent getShareIntent(){
		
		final String extra_text = txtShareMessage.getEditableText().toString();
		final String extra_subject = "SharedTest by AndroidWorld.it";
		
		
		Intent shareIntent = new Intent(Intent.ACTION_SEND);
		shareIntent.setType("text/plain");
		shareIntent.putExtra(Intent.EXTRA_SUBJECT, extra_subject);
		if(extra_text.length() > 0)
			shareIntent.putExtra(Intent.EXTRA_TEXT, extra_text);
		else
			shareIntent.putExtra(Intent.EXTRA_TEXT, "ShareTest by AndroidWorld.it");
        
		return shareIntent;
	}

	@Override
	public void onClick(View clickedView) {
		
		switch(clickedView.getId()){
		case R.id.btnShare:
			String text = txtShareMessage.getEditableText().toString();
			
			if(text != null  && text.length()>0){
				txtShareMessage.setError(null);
				startActivity(getShareIntent());
			}else
				txtShareMessage.setError("Inserisci un messaggio, prima di condividerlo!");
			break;
		}
		
	}

}
